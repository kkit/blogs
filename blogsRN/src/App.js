/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {MainNavigator} from "./Navigators";
import {MenuProvider} from "react-native-popup-menu";

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};

const MainContainer = createAppContainer(MainNavigator);

export default class App extends Component<Props> {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <MenuProvider skipInstanceCheck backHandler={true}>
                <MainContainer/>
            </MenuProvider>
        )
    }
}
