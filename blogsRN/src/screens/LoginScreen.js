import React from "react";
import {Alert, Button, StyleSheet, Text, TextInput, View} from "react-native";
import {login as doLogin, register} from "../RestClient";

class LoginScreen extends React.Component {
    static navigationOptions = {
        title: 'Login Screen',
    };

    constructor(props) {
        super(props);
        this.returnTo = this.props.navigation.getParam('returnTo');
        this.navParams = this.props.navigation.state.params;
        this.state = {
            token: "",
            registerMode: false,
            user : "",
            pass : "",
            repeatPass : "",
            email : "",
            fullName : ""
        };
        this.login = this.login.bind(this);
        this.register = this.register.bind(this);
    }

    render() {
        return (
            <View style={{padding:50, flexDirection:"column", backgroundColor:"green", flex:1, justifyContent: "center", alignItems: "stretch"}}>
                <TextInput style = {styles.input}
                           onChangeText={text => this.setState({user:text})}
                           autoCapitalize="none"
                           // onSubmitEditing={() => this.pass.focus()}
                           autoCorrect={false}
                           keyboardType='email-address'
                           returnKeyType="next"
                           placeholder='Username'
                           placeholderTextColor='rgba(225,225,225,0.7)'
                           defaultValue={this.state.user}/>

                <TextInput style = {styles.input}
                           onChangeText={text => this.setState({pass : text})}
                           returnKeyType="go"
                           placeholder='Password'
                           placeholderTextColor='rgba(225,225,225,0.7)'
                           secureTextEntry
                           defaultValue={this.state.pass}/>
                {
                    this.state.registerMode ?
                        <View>
                        <TextInput style = {styles.input}
                                   onChangeText={text => this.setState({repeatPass : text})}
                                   autoCapitalize="none"
                                   autoCorrect={false}
                                   returnKeyType="next"
                                   placeholder='Repeat password'
                                   placeholderTextColor='rgba(225,225,225,0.7)'
                                   secureTextEntry
                                   defaultValue={this.state.repeatPass}
                                   />
                        <TextInput style = {styles.input}
                                   onChangeText={text => this.setState({email : text})}
                                   autoCapitalize="none"
                                   autoCorrect={false}
                                   returnKeyType="next"
                                   placeholder='Email'
                                   placeholderTextColor='rgba(225,225,225,0.7)'
                                   defaultValue={this.state.email}
                                   />
                        <TextInput style = {styles.input}
                                   onChangeText={text => this.setState({fullName : text})}
                                   autoCapitalize="none"
                                   autoCorrect={false}
                                   returnKeyType="next"
                                   placeholder='Full name'
                                   placeholderTextColor='rgba(225,225,225,0.7)'
                                   defaultValue={this.state.fullName}
                                   />
                            <View style={{flexDirection: "row", justifyContent:"space-evenly"}}>
                                <Button title='Cancel' onPress={() => this.setState({registerMode: false, loginError: undefined})}/>
                                <Button title='Register' onPress={this.register}/>
                            </View>
                        </View> :
                        <View style={{flexDirection:"row", justifyContent:"space-evenly"}}>
                            <Button title='Register' onPress={() => this.setState({registerMode:true, loginError: undefined })}/>
                            <Button title='Login' onPress={this.login}/>
                        </View>
                }
                {this.state.loginError ? <Text style={{color:'red'}}>{this.state.loginError}</Text> : undefined}
            </View>
        )

    }



    register() {
        // check fields beforehand
        let err = "";
        if (!this.state.user) err = "username"; else
        if (!this.state.pass) err = "password"; else
        if (!this.state.repeatPass) err = "password"; else
        if (!this.state.email) err = "email"; else
        if (!this.state.fullName) err = "full name";
        if (err) {
            this.setState({loginError: "Field is empty: " + err});
            return;
        }
        if (!validateEmail(this.state.email)) {
            this.setState({loginError: "Email is incorrect"});
            return;
        }

        if (this.state.pass !== this.state.repeatPass) {
            this.setState({loginError: "Passwords don't match"});
            return;
        }


        register(this.state.user, this.state.pass, this.state.email, this.state.fullName)
            .then(() => {
                Alert.alert("Success", "Registration successful. You can log in now");
                this.setState({registerMode:false, loginError: undefined});
            })
            .catch((e) => {
                this.setState({loginError: e.message});
            })
    }

    login() {
        doLogin(this.state.user, this.state.pass)
            .then(token => {
                this.setState({loginError: undefined});
                this.props.navigation.navigate(this.returnTo, {authToken: token, ...this.navParams});
            })
            .catch((e) => {
                this.setState({loginError: e.message});
            })
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


const styles = StyleSheet.create({
    container: {
        padding: 20
    },
    input:{
        height: 40,
        backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: 10,
        padding: 10,
        color: '#fff'
    },
    buttonContainer:{
        backgroundColor: '#2980b6',
        paddingVertical: 15
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    }});

export default LoginScreen;
