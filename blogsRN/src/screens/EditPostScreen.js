import React from "react";
import {Alert, View, Button, Text, TextInput, Picker, StyleSheet, Image, ScrollView, KeyboardAvoidingView} from "react-native";
import {Header, Icon} from "react-native-elements";
import {getPictureSource, savePost} from "../RestClient";
import ImagePicker from "react-native-image-picker";
import {topics} from "../config";
import MultiSelect from "../components/react-native-multiple-select/react-native-multi-select";
import {parseTopics, stringifyTopics} from "../components/TopicIcon";

/* For imagePicker to work on android, add these to AndroidManifest.xml
    <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>

 */
export default class EditPostScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam("title", "") // thus we can update navigation header dynamically
        }
    };

    constructor(props) {
        super(props);
        this.state = {post: {title:"", description:"",content:""}, picture: {}, selectedItems:{}}
    }


    componentDidMount() {
        let post = this.props.navigation.getParam('post') || {};
        let token = this.props.navigation.getParam('token');
        this.setState({post: post, token: token, topic: post?.topic?.name || topics[0]});
        if (post) {
            this.props.navigation.setParams({title: 'Edit post: ' + post.title}); // thus we can update navigation header dynamically
            this.setState({picture: post.picture ? post.picture : undefined });
        }
        else
            this.props.navigation.setParams({title: 'Create post'});
    }

    render() {
        this.title = this.state.post?.title;
        this.description = this.state.post?.description;
        this.content = this.state.post?.content;
        const selectedItems = this.state.post ? parseTopics(this.state.post.topics) : [];

        return (
            <View style={{flexDirection: "column", backgroundColor: "#419bff", flex: 1, justifyContent: "center", alignItems: "stretch"}}>
                <Header style={{flex:1}} innerContainerStyles={{justifyContent: 'space-between', alignItems: 'center'}}
                        leftComponent={
                        <Image source={{uri: 'data:image/jpeg;base64,' + this.state.picture}} style={{width: 50, height: 50}}/>
                    }
                    centerComponent={
                        <View style={{flexDirection:"row", justifyContent:"space-between"}}>
                        <Icon reverse name="camera" type="material-community" color="green" size={20} onPress={this.selectImage}/>
                        <Icon reverse name="camera-off" type="material-community" color="red" size={20} onPress={this.clearImage} />
                        </View>
                    }
                    rightComponent={<Button title="Save" onPress={this.save}/>}
                />
                <MultiSelect
                    items={topics}
                    uniqueKey="name"
                    displayKey="name"
                    onSelectedItemsChange={this.onTopicSelect}
                    selectedItems={selectedItems}
                    hideSubmitButton
                    hideTags={true}
                    autoFocusInput={false}
                    selectText={"Select topics"}
                    tagBorderColor="white"
                    tagRemoveIconColor="red"
                    tagTextColor="white"
                    showSelectedAmount={false}

                />
                <Text style={{marginTop: -20}}>Topics selected: {this.state.post.topics?.replace(/\|\|/g,", ").replace(/\|/g, "")}</Text>
                    <TextInput style={{flex:2, ...styles.input}} defaultValue={this.state.post.title} onChangeText={text => this.updateText("title", text)}  placeholder="Title..."/>
                    <TextInput multiline style={{flex: 2, ...styles.input}} defaultValue={this.state.post?.description} onChangeText={text => this.updateText("description", text)}  placeholder="Short description"/>
                    <TextInput multiline style={{flex: 6, ...styles.input}} defaultValue={this.state.post?.content} onChangeText={text => this.updateText("content", text)}  placeholder="Content"/>
            </View>

        )
    }

    onTopicSelect = (topics) => {
        let post = this.state.post;
        post.topics = stringifyTopics(topics);
        this.setState({post: post});
    };

    updateText = (field, text) => {
        {
            this.setState(p => {
                p.post[field] = text;
                return {post: p.post}
            })
        }
    };

    save = () => {
        let post = this.state.post;
        post.topics = this.state.post.topics;
        if (!post.topics || post.topics.length ===0) {
            Alert.alert("Warning", "Select at least one topic");
            return;
        }
        post.picture = this.state.picture;
        if (!post.title || post.title.trim().length === 0) {
            Alert.alert("Warning", "Title must be specified");
            return;
        }
        if (!post.description || post.description.trim().length === 0) {
            Alert.alert("Warning", "Description must be specified");
            return;
        }
        if (!post.content || post.content.trim().length === 0) {
            Alert.alert("Warning", "No content");
            return;
        }

        savePost(post, this.state.token)
            .then(resp => {
                if (resp.ok) this.props.navigation.goBack();
                else return resp.json()
            }).then(json => JSON.stringify(json))
            .then(err => {if (err) Alert.alert("Error saving post", err)});
    };

    clearImage = () => {
        this.setState({
            picture : undefined,
            imageHeight : 0,
            imageWidth : 0
        })
    };

    selectImage = () => {
        const options = {
            title: 'Select Image',
            // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                // use Base64.btoa(response.data)

                this.setState({
                    picture : response.data,
                    imageHeight : response.height,
                    imageWidth : response.width
                });
            }
        });
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    input:{
        backgroundColor: 'white',
        margin:1,
        color: 'black',
        padding: 5
    },
    buttonContainer:{
        backgroundColor: '#2980b6',
        paddingVertical: 15
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    }});
