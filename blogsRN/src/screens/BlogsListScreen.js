import React from "react";
import styles from "./BlogsListScreen.Styles";
import {getBlogs, logout} from "../RestClient";
import {Alert, Dimensions, FlatList, View, SectionList} from "react-native";
import PostInList from "../components/PostInList";
import {Header, Icon, SearchBar, Text} from "react-native-elements";
import BlogListPopupMenu from "../components/BlogListPopupMenu";
import {autoLogin} from "../RestClient"

const PAGE_SIZE = 20;

export default class BlogsListScreen extends React.Component {
    static navigationOptions = {
        title: 'BLogs List',
    };

    constructor(props) {
        super(props);
        this.state = {
            blogs: [],
            token: "",
            currentUser: 0,
            refreshing: false,
            author: "",
            topic: "",
            pageNum:0,
            endReached:false
        };
        this.timeout = {};
        this.searchText = "";
    }

    componentDidMount() { // fetching blog posts
        // on activating we need to check if we're logged in. componentDidMount itself doesn't fire when we come here via navigation.goBack, for example
       this.mountListener = this.props.navigation.addListener("willFocus", payload => {
           autoLogin().then(token => this.setState({token: token}));
           this.refresh(); // suboptimal, needs to be optimized. this is needed after edit/create a post
       });
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
        this.mountListener.remove();
    }

    refresh = (add: boolean) => { // add==true - if just downloading a new portion of data. else - just get a new data instead of the old one
        if (this.state.refreshing) return; // or else we get some interesting issues
        this.setState({refreshing: true});
        getBlogs({topic: this.state.topic, author: this.state.author, pageNum: add ? this.state.pageNum : 0})
            .then(response => {
                if (response.ok)
                    return (response.json());
                throw new Error("Response status: " + response.status);
            })
            .then(json => {
                if (!add)
                    return this.setState({refreshing: false, blogs: json, pageNum: 1, endReached: json.length < PAGE_SIZE});
                else
                    return this.setState(prevState => {
                            let newBlogs = prevState.blogs.concat(json);
                            return {refreshing: false, blogs:newBlogs, pageNum: prevState.pageNum + 1, endReached: json.length < PAGE_SIZE}
                        }
                    )
            })
            .catch(err => {
                console.error(err);
                Alert.alert("Error loading posts", err);
                this.setState({refreshing: false, blogs: undefined});
            })
    };

    render() {
        // Generating sections
        let sections = new Map();
        this.state.blogs.forEach(post => {
            let postDate = new Date(post.createDate).toDateString();
            let p = sections.get(postDate) || [];
            p.push(post);
            sections.set(postDate, p);
        });
        let sectionsData = [];
        let odd = false;
        for (let key of sections.keys()) {
            sectionsData.push({title:key, odd:odd, data:sections.get(key)});
            odd = !odd;
        }


        return (
            <View style={styles.container}>
                <Header
                    outerContainerStyles={this.state.token ? styles.headerOuterLogged : styles.headerOuterAnonymous}
                    innerContainerStyles={styles.headerInner}
                    leftComponent={<BlogListPopupMenu
                                        loggedIn={!!this.state.token}
                                        onRefresh={this.refresh}
                                        onSetFilter={this.setTopicFilter}
                                        onLogin={this.login}
                                        onLogout={this.doLogout}
                    />}
                    centerComponent=<SearchBar
                    round
                    lightTheme
                    clearIcon={{name: 'clear'}}
                    placeholder="Search by Author"
                    containerStyle={styles.headerContainer}
                    inputStyle={{borderColor: "transparent", width: Dimensions.get("window").width-60}} // does any other way to set the width exist?
                    onChangeText={this.onChangeAuthorFilter}
                />
                />
                <SectionList
                    extraData={this.state} //extraData is needed for lists to react on state change. it's PureComponent, so doesn't do that automatically
                    renderItem={({item, index, section}) => <PostInList section={section} key={item.id} openPost={this.openPost} post={item}/>} // this key={section + item.id} is not needed in flatlist, but SectionList has some glitches if the key is absent
                    renderSectionHeader={({section:{title}}) => (<Text style={styles.sectionHeader}>{title}</Text>)}
                    sections={sectionsData}
                    keyExtractor={(item, index) => item? item.id?.toString() : "0"}
                    style={{paddingBottom : 70}} // because of Header, which hash height 70 by default
                    showsVerticalScrollIndicator={true}
                    refreshing = {this.state.refreshing}
                    onRefresh={this.refresh}
                    ListEmptyComponent={this.state.refreshing ? <Text>Loading...</Text> : <Text>No Data</Text>}
                    onEndReached={this.onEndReached}
                    onEndReachedThreshold={0.2}
                    stickySectionHeadersEnabled={true}
                />
                {this.state.token ?
                    <Icon raised name="announcement" type="" color="white" size={25}
                          iconStyle={{fontSize: 40}}
                          containerStyle={{backgroundColor: "red", position: "absolute", right: 30, bottom: 30}}
                          onPress={this.createPost}/>
                    : undefined
                }
            </View>
        )
    }

    /*
    note https://github.com/facebook/react-native/issues/9436
    about setTimeout working under debugger
     */
    onChangeAuthorFilter = (text) => {
        this.searchText = text;
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => this.setFilter(this.searchText), 400)
    };

    onEndReached = (info) => {
        // console.log("onEndReached. " + this.state.endReached);
        if (!this.state.endReached)
          this.refresh(true);
    };

    openPost = (id) => {
        // console.log("openPost " + id);
        this.props.navigation.navigate('ViewPost', {postID: id, authToken: this.state.token});
    };


    setFilter = (searchText) => {
        this.setState({author:searchText},  () => this.refresh(false));
    };

    setTopicFilter = (searchText) => {
        console.log("Search for topic: " + searchText.name);
        this.setState({topic:searchText.name}, () => this.refresh(false));

    };

    login = () => {
        this.props.navigation.navigate("Login", {returnTo: 'BlogsList'});
    };

    doLogout = () => {
        logout().then(this.setState({token:""}));
    };

    createPost = () => {
        this.props.navigation.navigate('EditPost', {token: this.state.token});
    }

}