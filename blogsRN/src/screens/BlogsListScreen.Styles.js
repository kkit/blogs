import {StyleSheet} from "react-native";

export default styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerOuterLogged: {
        backgroundColor: "#3D6DCC"
    },
    headerOuterAnonymous: {
        backgroundColor: "#cc4542"
    },
    headerInner: {
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    headerContainer: {
        backgroundColor: "transparent",
        borderColor: "transparent",
        borderBottomWidth: 0,
        borderTopWidth: 0
    },

    sectionHeader: {
        color:"red"
    }

});
