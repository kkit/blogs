import React from "react";
import {View, Text, ScrollView, Button, Image, Alert} from "react-native";
import {
    autoLogin,
    deletePost,
    getPost,
    getPostPicture,
    getPostWithPicture,
    logout,
    currentUserID,
    currentUserName
} from "../RestClient";
import TopicIcon from "../components/TopicIcon";
import {Divider, Header, Icon} from "react-native-elements";
import {Dimensions} from "react-native";

export default class ViewPostScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam("title", "") // thus we can update navigation header dynamically
        }
    };

    constructor(props) {
        super(props);
        this.state = {token: {}, post: {}, postPicture:{}, curUser: 0};
    }

    componentDidMount() {
        // this.setState({token: this.props.navigation.getParam('authToken')});
        currentUserName().then(name => this.setState({curUser:name}));
        this.mountListener = this.props.navigation.addListener("willFocus", payload => {
            autoLogin().then(token => this.setState({token: token}));
            let id = this.props.navigation.getParam('postID');
/*
            getPost(id)
                .then(resp => resp.json())
*/
            getPostWithPicture(id)
                .then(post => {
                    this.setState({post: post});
                    this.props.navigation.setParams({title: post.title}) // thus we can update navigation header dynamically
                });
        });

    }

    componentWillUnmount() {
        this.mountListener.remove();
    }

    editPost = () => {
        this.props.navigation.navigate('EditPost', {post: this.state.post, token: this.state.token});
    };

    deletePost = () => {
        deletePost(this.state.post.id, this.state.token)
            .then(resp => {
                if (!resp.ok) resp.text().then(text => Alert.alert(text))
                else this.props.navigation.goBack()
            })
    };

    doLogin = () => {
        this.props.navigation.navigate("Login", {returnTo: 'ViewPost', postID:this.state.post?.id})
    };

    doLogout = () => {
      logout().then(this.setState({token:"", curUser:undefined}));
    };

    render() {
        let id = this.props.navigation.getParam('postID');
        let post = this.state.post;
        //on the first render post is undefined, and we get an error. so we need here this '?' sign
        let createDate = new Date(post.createDate);
        let dimensions = Dimensions.get("window");
        // let the picture take the whole width. we need to calculate its size to save proportions
        let pictureExists = post && post.pictureWidth > 0;
        let pictureStyle = {};
        if (pictureExists) {
            let k = dimensions.width > dimensions.height ? dimensions.height / post.pictureHeight : dimensions.width / post.pictureWidth;
            pictureStyle = {width : post.pictureWidth * k, height: post.pictureHeight * k, resizeMode: "stretch"}
        }
        return (
            <ScrollView>
                <Header
                    outerContainerStyles={{backgroundColor: '#3D6DCC'}}
                    innerContainerStyles={{justifyContent: 'space-between', alignItems: 'center'}}
                    leftComponent={
                        <View style={{flexDirection: "row", justifyContent: "space-evenly", alignItems: "center"}}>
                            <Icon name="chevron-left" type="material" color="white" size={40} onPress={() => this.props.navigation.goBack()}/>
                        </View>
                    }
                    rightComponent={
                        (this.state.token ?
                                <Button title="logout" onPress = {this.doLogout}/>
                                :
                                <Button title="login" onPress = {this.doLogin}/>
                        )
                    }
                    centerComponent={
                        <View style={{flexDirection:"row", justifyContent:"space-between"}}>
                            {(this.state.curUser === post?.author?.username) && this.state.token ?
                                <>
                                <Icon reverse name="pencil" type="material-community" color="green" size={20} onPress={this.editPost}/>
                                <Icon reverse name="trash-can-outline" type="material-community" color="red" size={20} onPress={() => ViewPostScreen.confirmation('Really delete this post?', this.deletePost)}/>
                                </>
                                :
                                undefined
                            }
                        </View>

                    }
                />
                <Text numberOfLines={2} ellipsizeMode="tail" style={{fontSize:10, fontStyle:"italic"}}>Topics: {post.topics?.replace(/\|\|/g,", ").replace(/\|/g, "")}</Text>
                <Text numberOfLines={1} ellipsizeMode="tail" style={{fontSize:12, fontStyle:"italic"}}>Posted: {createDate?.toUTCString()} By {post.author?.fullName}</Text>
                <Text numberOfLines={3} ellipsizeMode="tail" style={{fontSize:20, fontWeight: "bold"}}>{post.title}</Text>
                <Divider/>
                <Text style={{fontSize:14, fontStyle: "italic"}} >{post.description}</Text>
                {pictureExists ?
                    <Image source={{uri:'data:image/jpeg;base64,' + this.state.post.picture}} style={{marginTop:10, marginBottom:10, ...pictureStyle}}/>
                    : undefined
                }
                <Text >{post.content}</Text>
            </ScrollView>
        )
    }

    static confirmation(message, func) {
      Alert.alert("Confirmation", message, [
          {text: "Delete", onPress: func},
          {text: "Cancel"}
      ]);
    }

}