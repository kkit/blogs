import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View} from "react-native";
import {Icon} from "react-native-elements";

export function getIcon(topic) {
    let iconName, type;
    let name = topic?.name;
    switch (name) {
        case "Fashion" :
            iconName = "redeem";
            type = "action";
            break;
        case "Food" :
            iconName = "food";
            type = "material-community";
            break;
        case "Health" :
            iconName = "man";
            type = "entypo";
            break;
        case "Music" :
            iconName = "music";
            type = "font-awesome";
            break;
        case "Nature" :
            iconName = "nature";
            type = "material-community";
            break;
        case "News" :
            iconName = "news";
            type = "entypo";
            break;
        case "Science" :
            iconName = "atom";
            type = "material-community";
            break;
        case "Sports" :
            iconName = "lifebuoy";
            type = "entypo";
            break;
        case "Travel" :
            iconName = "directions-walk";
            type = "material";
            break;
        default:
            iconName = "comment-question-outline";
            type = "material-community";
    }
    return {iconName, type};
}

class TopicIcon extends Component {
    render() {
        let {iconName, type} = getIcon(this.props.topic);
        return (
            <Icon {...this.props} name={iconName} type={type} />
        );
    }
}

TopicIcon.propTypes = {topic : PropTypes.object};

export default TopicIcon;


// takes a string of topics like "|Travel||Fashion||Fashion||Science||Science||Science||Science||News|"
// and makes an array of topics, as in config.js
export function parseTopics(topics) {
    if (!topics || topics.length<=1) return [];
    let strs = topics.split("||");
    let res=[];
    strs.forEach((item, index) => {res.push(item.replace(/\|/g,""))});
    return res;
}

// and vice versa
export function stringifyTopics(topics) {
    if (!topics || !topics.length || topics.length===0) return "";
    return "|" + topics.join("||") + "|";
}