import React from "react";
import {View} from "react-native";
import PropTypes from 'prop-types';
import {Menu, MenuOptions, MenuOption, MenuTrigger, renderers} from "react-native-popup-menu";
import {Divider, Icon, Text} from "react-native-elements";
import {topics} from "../config.js"
import TopicIcon, {getIcon} from "./TopicIcon";

class BlogListPopupMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.topics = {};
    }

    render() {
        this.topics = topics;
        return (
            <Menu renderer={renderers.Popover} rendererProps={{placement: "bottom"}}>
                <MenuTrigger><Icon name="menu" color="white"/></MenuTrigger>
                <MenuOptions style={{width: 150}}>
                    <MenuOption text="Refresh" onSelect={this.props.onRefresh}/>
                    <Divider/>
                    <MenuOption text="Filter by topic:" onSelect={undefined}/>
                    <MenuOption key={0} style={{marginLeft: 20}}
                                onSelect={() => this.props.onSetFilter("")}>
                        <View style={{flexDirection: "row", justifyContent: "flex-start"}}>
                            <TopicIcon topic={undefined} iconStyle={{width:30}}/>
                            <Text>Show all</Text>
                        </View>
                    </MenuOption>
                    {
                        this.topics.map(topic => {
                            return (<MenuOption key={topic.name} style={{marginLeft: 20}}
                                               onSelect={() => this.props.onSetFilter(topic)}>
                                <View style={{flexDirection: "row", justifyContent: "flex-start"}}>
                                <TopicIcon topic={topic} iconStyle={{width:30}}/>
                                <Text>{topic.name}</Text>
                                </View>
                            </MenuOption>)
                        })
                    }
                    <Divider/>
                    {
                        this.props.loggedIn ?
                            <MenuOption key="logout" text={"Logout"} onSelect={this.props.onLogout}/> :
                            <MenuOption key="login" text={"Log in"} onSelect={this.props.onLogin}/>
                    }
                </MenuOptions>

            </Menu>
        );
    }
}

BlogListPopupMenu.propTypes = {
    onRefresh: PropTypes.func.isRequired,
    onSetFilter: PropTypes.func.isRequired,
    onLogout: PropTypes.func.isRequired,
    onLogin: PropTypes.func.isRequired,
    loggedIn: PropTypes.bool.isRequired
};

export default BlogListPopupMenu;