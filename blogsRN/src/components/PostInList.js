import React from "react";
import {StyleSheet, Text, View, Image} from "react-native";
import {Header, ListItem} from "react-native-elements";
import TopicIcon from "./TopicIcon";
import config from "../config";

export default class PostInList extends React.Component {
    constructor(props) {
        super(props);
    }

    onPress = () => {
        this.props.openPost(this.props.post.id);
    };

    render() {
        let post = this.props.post;
        let section = this.props.section;
        if (!post) return null;
        return (
            <ListItem containerStyle={{backgroundColor:(section.odd ? "white" : "#d2d2d2")}}
                leftIcon={<Image source={{uri:'data:image/jpeg;base64,' + post.thumb}} style={{resizeMode:"contain", width:config.thumbSize, height:config.thumbSize, marginRight: 10}}/>}
                onPress={this.onPress}
                title={
                    <View style={{flexDirection:"row", justifyContent:"flex-start", alignItems:"center"}}>
                    <Text style={{fontSize:12, fontStyle:"italic", marginRight: 10}}>{post.author?.fullName}</Text>
                    <Text>{post.title}</Text>
                    </View>
                }
                subtitle={
                    <View>
                        <Text>{post.description}</Text>
                    </View>
                }
            />
        )
    };
}

const styles = StyleSheet.create({
    Text: {
        backgroundColor: '#dd5251',
        padding: 10
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    button: {
        alignItems: 'center',
        padding: 10
    },
    countContainer: {
        alignItems: 'center',
        padding: 10
    },
    countText: {
        color: '#FF00FF'
    }
});