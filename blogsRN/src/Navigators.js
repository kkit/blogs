import React from 'react';
import {createMaterialTopTabNavigator, createStackNavigator, createSwitchNavigator} from 'react-navigation';
import LoginScreen from "./screens/LoginScreen";
import BlogsListScreen from "./screens/BlogsListScreen";
import ViewPostScreen from "./screens/ViewPostScreen";
import EditPostScreen from "./screens/EditPostScreen";

// !!! In order for createStackNavigator to work, run `react-native.cmd link react-native-gesture-handler`
// from the command line, then rebuild the app.
export const BlogsNavigator = createStackNavigator({
        BlogsList: BlogsListScreen,
        ViewPost: ViewPostScreen,
        EditPost: EditPostScreen
    }, {
        headerMode: 'none'
    }
);

export const MainNavigator = createSwitchNavigator({
        BlogList: BlogsNavigator,
        Login: LoginScreen,
    }
);