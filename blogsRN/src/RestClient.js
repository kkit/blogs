import config from "./config"
import {Base64} from 'js-base64'; // npm install js-base64 --save to have this
import {AsyncStorage} from "react-native";
import RNFetchBlob from "rn-fetch-blob";

export async function login(user, pass) {
    const header = new Headers();
    header.append("Accept", "application/json");
    header.append("Authorization", "Basic " + Base64.btoa(config.clientId + ":" + config.secret));
    let res = await fetch(config.tokenURL + "/oauth/token?grant_type=password&username=" + user + "&password=" + pass, {
        method: "POST",
        headers: header
    });
    if (!res.ok) {
        res.text().then(t => console.log("Error while logging in: " + t));
        throw new Error("Incorrect username or password");
    }
    let json = await res.json();
    console.log("Got token: " + JSON.stringify(json));
    await AsyncStorage.multiSet([["access_token", json.access_token],
        ["refresh_token", json.refresh_token],
        ["user_id", json.id.toString()],
        ["user_name", user]
    ]);
    return await json.access_token;
}

export async function register(username, pass, email, fullName) {
    let body = {
        username: username,
        password: pass,
        email: email,
        fullName: fullName
    };
    let res = await fetch(config.url + "user", {method: "POST", headers: headers(), body: JSON.stringify(body)});
    if (!res.ok)
      throw new Error("Registration error: " + await res.text());
}


export function autoLogin() {
    return AsyncStorage.getItem("access_token");
}

export function logout(params) {
    return AsyncStorage.multiRemove(["access_token", "refresh_token", "user_id"])
}

// currently logged user
export function currentUserName() {
  return AsyncStorage.getItem("user_name");
}

export function getBlogs(params) {
    let {topic, author, pageNum} = params || {topic: "", params: "", pageNum: 0};
    let q = [];
    if (topic) q.push("topic="+encodeURIComponent(topic));
    if (author) q.push("author="+encodeURIComponent(author));
    q.push("page="+pageNum);
    let uri =config.url + "blogs";
    if (q.length > 0) uri = uri + "?" + q.join("&");
    return fetch(uri, {method: "GET", headers: headers()})
}

export function getPost(id) {
    return fetch(config.url + "blogs/" + id, {method: "GET", headers: headers()});
}

export function savePost(post, token) {
    // todo errors processing
    // actually, according to http standards, POST is used to create new data, PUT to update the existing one.
    // but here only one endpoint will be used
    return fetch(config.url + "blogs", {method: "POST", headers: headers(token), body: JSON.stringify(post)});
}
export function deletePost(id, token) {
    // todo errors processing
    return fetch(config.url + "blogs/" + id, {method: "DELETE", headers: headers(token)});
}

export async function getPostWithPicture(id) {
    try {
        let resp = await getPost(id);
        if (resp.ok) {
            let post = await resp.json();
            post.picture = await getPictureAsBase64(id);
            return post
        }
    } catch (e) {
        console.error("getPostWithPicture error");
        console.error(e);
    }
}

// on big files (like 3+Mb this works pretty slow. in the future it would be better to get existing files by url directly from server,
// as we did it before
export function getPictureAsBase64(id) {
    return RNFetchBlob.fetch("GET",
        config.url + "blogs/" + id + "/image?r=" + Math.random())
        .then(res => res.base64())
        .catch((msg, code) => {
            console.log("Error getting picture: " + msg + ", code: " + code);
    })
}

export function getPictureSource(id) {
    return {
        // by default it caches all images and never gets them from server again. meanwhile, an image in a post could by changed.
        // for now we'll just add randomness to the image's url
        uri: config.url + "blogs/" + id + "/image?r=" + Math.random(),
        method: "GET",
        headers:  headers()
    }
}

function headers(token) {
    const header = new Headers();
    header.append("Accept", "application/json");
    header.append("Content-Type", "application/json");
    if (token) header.append("Authorization", "Bearer " + token);
    return header;
}