export default {
    clientId: "blogsClient",
    secret: "blogSecret",
    tokenURL: "http://192.168.0.6:8080",
    url: "http://192.168.0.6:8080/api/",
    thumbSize: 40 // size of thumb image in posts list
}

// todo take this from the server
export const topics = [
    {name: "Fashion"},
    {name: "Food"},
    {name: "Health"},
    {name: "Music"},
    {name: "Nature"},
    {name: "News"},
    {name: "Science"},
    {name: "Sports"},
    {name: "Travel"}
];