import config from "./config"
import credentialStorage from "./components/credential-storage";

export async function login(user, pass) {
    const header = new Headers();
    header.append("Accept", "application/json");
    header.append("Authorization", "Basic " + btoa(config.clientId + ":" + config.secret));
    let res = await fetch(config.tokenURL + "/oauth/token?grant_type=password&username=" + user + "&password=" + pass, {
        method: "POST",
        headers: header
    });
    if (!res.ok) {
        res.text().then(t => console.log("Error while logging in: " + t));
        throw new Error("Incorrect username or password");
    }
    let json = await res.json();
    console.log("Got token: " + JSON.stringify(json));
    saveCredentials(json);
}

export async function loginOAuth(response) {
    // check response, extract tokens, user name etc., save credentials
    if (response.error)
        throw new Error(response.error);
    // having id token we can now authorize on our server
    const header = new Headers();
    header.append("Accept", "application/json");
    header.append("Authorization", "Basic " + btoa(config.clientId + ":" + config.secret));
    let res = await fetch(config.tokenURL + "/oauth/token?grant_type=google&google_id_token="+response.getAuthResponse().id_token, {
        method: "POST",
        headers: header
    });
    if (!res.ok) {
        res.text().then(t => console.log("Error while logging in: " + t));
        throw new Error("Error while logging in");
    }
    let json = await res.json();
    console.log("Got token: " + JSON.stringify(json));
    saveCredentials(json);

    /*
    response.getAuthResponse().id_token
response.getAuthResponse().expires_at
response.getAuthResponse().access_token

response.getBasicProfile().getId()
response.getBasicProfile().getEmail()
response.getBasicProfile().getName()
response.getBasicProfile().getImageUrl()


     */
}

export function saveCredentials(json) {
    credentialStorage.access_token = json.access_token;
    credentialStorage.refresh_token = json.refresh_token;
    credentialStorage.userId = json.id.toString();
    credentialStorage.userDisplayName = json.fullName;
    credentialStorage.userEmail = json.email;
    // don't think it's necessary, but let it be for a while
    credentialStorage.provider = json.provider;
    credentialStorage.providerUserId = json.provider_id;
    credentialStorage.storeCredentials();
}

export async function register(username, pass, email, fullName) {
    let body = {
        username: username,
        password: pass,
        email: email,
        fullName: fullName
    };
    let res = await fetch(config.url + "user", {method: "POST", headers: headers(), body: JSON.stringify(body)});
    if (!res.ok)
        throw new Error("Registration error: " + await res.text());
}

export function autoLogin() {
    credentialStorage.loadCredentials();
    return credentialStorage;
}

export function logout() {
    console.log("Logout");
    credentialStorage.clearCredentials();
}

function headers(token) {
    const header = new Headers();
    header.append("Accept", "application/json");
    header.append("Content-Type", "application/json");
    if (token) header.append("Authorization", "Bearer " + token);
    return header;
}
export function getBlogs(params, token) {
    let {topic, author, pageNum} = params || {topic: "", params: "", pageNum: 0};
    let q = [];
    if (topic) q.push("topic="+encodeURIComponent(topic));
    if (author) q.push("author="+encodeURIComponent(author));
    q.push("page="+pageNum);
    let uri =config.url + "blogs";
    if (q.length > 0) uri = uri + "?" + q.join("&");
    return fetch(uri, {method: "GET", headers: headers(token)})
}

export function getPost(id) {
    return fetch(config.url + "blogs/" + id, {method: "GET", headers: headers()});
}

export function savePost(post, token) {
    // todo errors processing
    // actually, according to http standards, POST is used to create new data, PUT to update the existing one.
    // but here only one endpoint will be used
    return fetch(config.url + "blogs", {method: "POST", headers: headers(token), body: JSON.stringify(post)});
}

export function deletePost(id, token) {
    // todo errors processing
    return fetch(config.url + "blogs/" + id, {method: "DELETE", headers: headers(token)});
}

export function validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

export function getPostImageURL(post) {
    return config.url + "blogs/" + post.id + "/image?r="+Math.random();
}

