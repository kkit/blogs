import React, {Component} from 'react';
import {deletePost, getPost, getPostImageURL} from "../RestClient";
import {AuthContext} from "../components/auth-context";
import {Link} from "react-router-dom";
import {Button, Modal, ModalBody, ModalFooter} from "reactstrap";
import credentials from "../components/credential-storage";

class ViewPostScreen extends Component {
    static contextType = AuthContext;

    constructor(props) {
        super(props);
        this.state = {post: undefined, loading: true, deleteQuestion: false};
    }

    componentDidMount() {
        let postID = this.props.match.params.id;
        if (postID) {
            this.setState({loading: true})
            getPost(postID)
                .then(response => {
                    if (response.ok) return response.json()
                }).then(json => {
                this.setState({post: json, loading: false});
            })
        }
    }

    render() {
        let p = this.state.post;
        if (this.state.loading) return "Loading";
        if (p) {
            document.title = p.title;
            // style={{overflow:"auto"}} helps the screen to have separate scroller from header. now the header doesn't scroll away when we scroll the page
            return (
                <div style={{overflow:"auto"}} className="mt-3">
                    {p.author.id.toString()!==credentials.userId || (<><Link to={"/post/edit/" + p.id}>Edit Post</Link> <Button className="btn btn-danger" onClick={this.showDelete}>Delete post</Button></>)}
                    <p>{p.id}</p>
                    <p><img src={getPostImageURL(p)} alt="" className="post-image"/></p>
                    <p>{p.createDate}</p>
                    <p>{p.title}</p>
                    <p>{p.description}</p>
                    <p>{p.content}</p>
                    <p>{p.author.fullName}</p>
                    <p>{p.topics}</p>
                    <Modal isOpen={this.state.deleteQuestion} toggle={this.hideDelete} autoFocus={true}>
                        <ModalBody>Really delete this post?</ModalBody>
                        <ModalFooter>
                            <Button color="danger" onClick={this.deletePost}>Yes, delete post</Button>
                            <Button color="primary" onClick={this.hideDelete}>No, don't delete</Button>
                        </ModalFooter>
                    </Modal>

                </div>
            );
        }
        return "Post not found";
    }

    showDelete = () => {
        this.setState({deleteQuestion: true});
    };

    hideDelete = () => {
        this.setState({deleteQuestion: false});
    };


    deletePost = () => {
        deletePost(this.state.post.id, credentials.access_token)
            .then(resp => {
                if (!resp.ok) resp.text().then(text => console.log(text))
                else window.location="/";
            })

    }


}

export default ViewPostScreen;