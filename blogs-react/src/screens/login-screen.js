import React from 'react';
import {AuthContext} from "../components/auth-context";
import {Button, Form, Input} from "reactstrap";
import {register, validateEmail} from "../RestClient";
import GoogleLogin from "react-google-login";
import config from "../config";

export default class LoginScreen extends React.Component {
    // noinspection JSUnusedGlobalSymbols
    static contextType = AuthContext;

    constructor(props) {
        super(props);
        this.state = {
            loginError: "",
            registerMode: false,
            login:"",
            passw:"",
        };
    }

    setRegisterMode = (mode) => {
      this.setState({registerMode:mode, loginError:""});
    };

    login = (event) => {
        this.setState({loginError: ""});
        event.preventDefault();
/*  this doesn't work on IE11, will need a polyfill
        let formData = new FormData(event.target);
        let login = formData.get("login");
        let passw = formData.get("password");
*/
        this.context.login(this.state.login, this.state.passw)
            .then(() => this.props.history.goBack())
            .catch((e) => this.setState({loginError: e.message}))
    };

    googleLogin= (response) => {
        this.setState({loginError: ""});
        this.context.loginOAuth(response)
            .then(() => this.props.history.goBack())
            .catch((e) => this.setState({loginError: e.message}));
    };

    googleLoginFailed= (response) => {
        console.log(response);
        this.setState({loginError: response.error});
    };

    register = (event) => {
        event.preventDefault();
        let err = "";
        if (!this.state.login) err="Login is empty"; else
        if (!this.state.passw) err="Password is empty"; else
        if (this.state.passw!==this.state.repeatpassw) err="Passwords don't match"; else
        if (!this.state.fullName) err="Full Name is empty"; else
        if (!validateEmail(this.state.email)) err="Email is incorrect";
        this.setState({loginError: err});
        if (err) return;

        register(this.state.login, this.state.passw, this.state.email, this.state.fullName)
            .then(() => {
                alert("Registration successful. You can log in now");
                this.setState({registerMode:false, loginError: undefined});
            })
            .catch(error => {
                this.setState({loginError: error.message});
            })
    };

    updateState = (event) => {
        this.setState({[event.target.name]:event.target.value});

    };

    render() {
        return (
            <div className="flex-grow-1 d-flex justify-content-center align-items-center">
                {
                    this.state.registerMode ?
                        <RegisterForm setRegMode={this.setRegisterMode} updateState={this.updateState}
                                      loginError={this.state.loginError} register={this.register}/> :
                        <LoginForm setRegMode={this.setRegisterMode} login={this.login}
                                   loginError={this.state.loginError} updateState={this.updateState}
                                   googleLogin={this.googleLogin} googleLoginFailed={this.googleLoginFailed}/>
                }
            </div>
        )
    }

}

function LoginForm(props) {
    return(
        <Form onSubmit={props.login} className="login-form d-flex flex-column">
            <Input name="login" placeholder="Login" autoComplete="username" onChange={props.updateState}/>
            <Input type="password" name="passw" placeholder="Password" autoComplete="current-password" onChange={props.updateState}/>
            {props.loginError &&
            <p className="text-danger">{props.loginError}</p>}
            <div className="flex-grow-1 d-flex align-self-stretch justify-content-around align-items-end mt-3">
                <Button className="shadow-sm btn btn-link" onClick={() => props.setRegMode(true)} >Register</Button>
                <Button className="shadow-sm">Login</Button>
            </div>
            <div className="flex-grow-1 d-flex align-self-stretch justify-content-around align-items-end mt-3">
                <GoogleLogin
                    clientId={config.googleClientID}
                    onSuccess={props.googleLogin}
                    onFailure={props.googleLoginFailed}
                />
            </div>
        </Form>)
}

function RegisterForm(props) {
    return (
        <Form onSubmit={props.register} className="register-form d-flex flex-column">
            <Input name="login" id="i" className="form-control" placeholder="Login" onChange={props.updateState}/>
            <Input type="password" name="passw" placeholder="Password" onChange={props.updateState}/>
            <Input type="password" name="repeatpassw" placeholder="Repeat Password" onChange={props.updateState}/>
            <Input type="" name="fullName" placeholder="Full Name" onChange={props.updateState}/>
            <Input type="email" name="email" placeholder="Email" onChange={props.updateState}/>
            {props.loginError &&
            <p className="text-danger">{props.loginError}</p>}
            <div className="flex-grow-1 d-flex align-self-stretch justify-content-around align-items-end">
                <Button className="btn btn-link" onClick={() => props.setRegMode(false)}>Login</Button>
                <Button className="btn btn-success">Register</Button>
            </div>
        </Form>
    );
}


