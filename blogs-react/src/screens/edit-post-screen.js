import React, {Component} from 'react';
import {AuthContext} from "../components/auth-context";
import {getPost, getPostImageURL, savePost} from "../RestClient";
import {Button, Container, Form, Input, Label, UncontrolledAlert} from "reactstrap";
import TopicsSelect from "../components/topics-select";
import credentials from "../components/credential-storage";

class EditPostScreen extends Component {
    static contextType = AuthContext;

    constructor(props) {
        super(props);
        this.state = {post: {}, loading: false, error: ""};
        this.imageRef = React.createRef();
    }

    componentDidMount() {
        let postId = this.props.match && this.props.match.params.id;
        if (postId) {
            this.setState({loading: true});
            getPost(postId)
                .then(response => {
                    if (response.ok) return response.json()
                }).then(json => {
                this.setState({post: json, loading: false, imageURL: getPostImageURL(json), ...json});
            })
        }
    }

    submit = (event) => {
        this.setState({error: ""});
        event.preventDefault();
        let post={};
        // here' s the problem: we need to send an image back to the server as base64 encoded string
        // if we changed it here, then <img> element contains url to local blob, which we can transfer using FileReader
        // it's async, so save in callback
        let reader = new FileReader();
        if (this.state.imageObject) {
            reader.onloadend = () => {
                let str = reader.result; // base64 encoded. result starts with "image/jpeg;base64,", we need to remove it.
                post.picture = str.substring(str.indexOf("base64,") + "base64,".length);
                this.savePost(post);
            };
            reader.readAsDataURL(this.state.imageObject);
        } else {
            // but if it was just loaded from server, then <img> stores only http:// url. then we need to redraw it locally
            // and only after that take a base64 string
            if (this.state.id)
              post.picture = this.getBase64Image(post, this.imageRef.current);
            this.savePost(post);
        }
    };

// take <img> element and redraw its content to canvas. then take base64 encoded data of that drawing
    // thanks to https://stackoverflow.com/a/22172860
    // now only jpeg format is supported. it's because of creating thumbnail on the server.
    // it's not that difficult to implement any other formats, but not needed right now.
    getBase64Image = (post, img) => {
        const canvas = document.createElement("canvas");
        canvas.width = this.state.post.pictureWidth;
        canvas.height = this.state.post.pictureHeight;
        const ctx = canvas.getContext("2d", {alpha:false});
        ctx.fillStyle="white";
        ctx.drawImage(img, 0, 0);
        const dataURL = canvas.toDataURL("image/jpeg");
        return dataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
    };

    savePost(post) {
        post.id = this.state.id;
        post.title = this.state.title;
        post.description = this.state.description;
        post.content = this.state.content;
        post.topics = this.state.topics;
        let error = "";
        if (!post.topics) error = "topics"; else if (!post.title) error = "title"; else if (!post.description) error = "description"; else if (!post.content) error = "content";
        if (error)
            this.setState({error: error});
        else
            savePost(post, credentials.access_token)
                .then(resp => {
                    if (resp.ok) this.goBack();
                    else throw new Error(resp);
                })
                .catch(err => {
                    this.setState({error: "Error saving post: " + err})
                })
    }

    goBack = () => {
            this.props.history.goBack()
    };

    updateState = (event) => {
        this.setState({[event.target.name]:event.target.value});
    };

    setTopics = (value) => {
        this.setState({topics: value});
    };

    setFile = (event) => {
        if (event.target.files && event.target.files[0] && event.target.files[0].type==="image/jpeg") {
            this.setState({imageURL: URL.createObjectURL(event.target.files[0]), imageObject:event.target.files[0]})
        }
    };

    onDrop = (event) => {
        console.log("drop to " + event.target);
        event.preventDefault();
        if (event.dataTransfer.items) {
            if (event.dataTransfer.items[0].kind==='file') {
                let file = event.dataTransfer.items[0].getAsFile();
                if (file.type==="image/jpeg")
                  this.setState({imageURL: URL.createObjectURL(file), imageObject:file})
            }
        }
        this.setState({dragOverMessage: ""});
    };

    onDragEnter = (event) => {
        event.preventDefault();
        this.setState({dragOverMessage: "Drop here to change the picture"});
    };
    onDragLeave = (event) => {
        event.preventDefault();
        this.setState({dragOverMessage: ""});
    };
    // this one is needed needed to drop not directly on the div, but all its children also
    onDragOver = (event) => {
        event.preventDefault();
    };

    render() {
        let p = this.state.post || {};
        if (this.state.loading) return "Loading";
        document.title = p.title || "New Post";
        // style={{overflow:"auto"}} helps the screen to have separate scroller from header. now the header doesn't scroll away when we scroll the page
        // crossOrigin="Anonymous" needs for "getBase64Image" function to work. Else we'd get "Tainted canvases may not be exported" error
        return (
            <div style={{overflow: "auto"}} className="p-2 d-flex flex-column justify-content-start h-100">
                <Form onSubmit={this.submit} className="d-flex flex-column justify-content-between align-items-stretch h-100">
                    <div className="d-flex flex-md-row flex-column justify-content-between align-items-stretch" onDragOver={this.onDragOver} onDrop={this.onDrop}
                         onDragEnter={this.onDragEnter} onDragExit={this.onDragLeave}>
                        <div className="d-flex flex-column align-items-center">
                        <img crossOrigin="Anonymous" ref={this.imageRef} alt="" src={this.state.imageURL} className="post-image align-self-center"/>
                        <Input style={{display:"none"}} type="file" id="file" accept=".jpg, .jpeg" onChange={this.setFile}/>
                            <p>{this.state.dragOverMessage}</p>
                            <div className="d-flex flex-row align-items-start">
                            <label htmlFor="file" className="btn btn-success">Upload</label>
                            <Button className="btn btn-danger" onClick={() => {this.setState({imageURL: undefined})}}>Delete image</Button>
                            </div>
                        </div>
                        <div  className="flex-grow-1 d-flex flex-column align-items-stretch">
                            <Label for="topics" className="">Topics</Label>
                            <TopicsSelect className="flex-grow-1" invalid={this.state.error === "topics"} id="topics" name="topics"
                                          selected={p.topics} onChange={this.setTopics}/>
                        </div>
                        <div className="flex-grow-1 d-flex flex-column align-items-stretch">
                            <Label  className="">Title</Label>
                            <Input type="textarea" className="flex-grow-1" invalid={this.state.error==="title"} name="title" placeholder="Title" defaultValue={p.title} onChange={this.updateState}/>
                        </div>
                        <div className="flex-grow-1 d-flex flex-column align-items-stretch">
                            <Label  className="">Description</Label>
                            <Input type="textarea" className="flex-grow-1" invalid={this.state.error==="description"} name="description" placeholder="Description" defaultValue={p.description} onChange={this.updateState}/>
                        </div>
                    </div>
                    <Label  className="align-self-start">Content</Label>
                    <Input invalid={this.state.error==="content"} name="content" type="textarea" style={{minHeight: 300}} className="flex-grow-1" placeholder="Content" defaultValue={p.content} onChange={this.updateState}/>
                    <p>{!this.state.error ||
                    <UncontrolledAlert className="alert-danger">Field '{this.state.error}' not set</UncontrolledAlert>}</p>
                    <Container>
                        <Button className="btn btn-success">Submit</Button>
                        <Button className="btn btn-link" onClick={this.goBack}>Cancel</Button>
                    </Container>
                </Form>
            </div>
        );
    }
}

export default EditPostScreen;