import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Alert} from "reactstrap";
import {Link} from "react-router-dom";

class PostInList extends Component {
    static propTypes = {
        post: PropTypes.object.isRequired
    };


    constructor(props) {
        super(props);
        this.state = {post: undefined};
    }

    render() {
        let post = this.props.post;
        let date = new Date(post.createDate);
        return (
            <Alert className="alert alert-heading m-1">
                {date.toLocaleDateString() + " " + date.toLocaleTimeString() + " "}
                <img src={"data:image/jpeg;base64," + post.thumb} alt=""/>
                <Link to={"post/"+post.id}>{post.title}</Link>
                {"  " + post.author.fullName}
            </Alert>
        );
    }
}

export default PostInList;