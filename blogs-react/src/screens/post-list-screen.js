import React from "react";
import {getBlogs} from "../RestClient";
import PostInList from "./post-in-list";
import InfiniteScroll from "react-infinite-scroll-component";
import {AuthContext} from "../components/auth-context";
import credentials from "../components/credential-storage";

const PAGE_SIZE = 20;

export default class PostsListScreen extends React.Component {
    static contextType = AuthContext;

    constructor(props) {
        super(props);
        this.state = {
            blogs: [],
            pageNum:0,
            endReached:false
        };
    }

    componentWillUnmount() {
    }

    componentDidMount() {
        this.refresh();
    }

    //getSnapshotBeforeUpdate() - use to save/restore scroll position

    // if filterByAuthor in context was changed, then we need to refresh posts list
    componentWillReceiveProps(nextProps, nextContext) {
        if (nextContext.filterByAuthor !== this.context.filterByAuthor || nextContext.filterByTopic !== this.context.filterByTopic) {
            this.setState({pageNum:0, blogs:[]}, this.refresh);
        }
    }

    refresh = () => {
        console.log("Loading page " + this.state.pageNum);
        getBlogs({pageNum: this.state.pageNum, author: this.context.filterByAuthor, topic: this.context.filterByTopic}, credentials.access_token).then(
            response => {
                if (response.ok)
                    return (response.json());
                throw new Error("Response status: " + response.status);
            }
        ).then(json => {
            this.setPosts(json);
        }).catch(err => {
            console.error(err);
            // Alert.alert("Error loading posts", err);
            this.setState({blogs: undefined});
        })
    };

    setPosts(json) {
        this.setState(prevState => {
                let newBlogs = [...prevState.blogs, ...json];
                return {blogs: newBlogs, pageNum: prevState.pageNum + 1, endReached: json.length < PAGE_SIZE}
            }
        )
    }

    render() {
        let posts = this.state.blogs;
        if (posts) {
            return (
                <div id="scrollDiv" style={{overflow: "auto"}}>
                    <InfiniteScroll
                        dataLength={posts.length}
                        next={this.refresh}
                        hasMore={!this.state.endReached}
                        loader={<div key={0}>Loading...</div>}
                        endMessage={<div>No more posts</div>}
                        scrollThreshold="100px"
                        scrollableTarget="scrollDiv"
                    >
                        {posts.map(b => <PostInList {...this.props} key={b.id} post={b}/>)}
                    </InfiniteScroll>
                </div>
            );
        } else
            return "No data";
    }
}