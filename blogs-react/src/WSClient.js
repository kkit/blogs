// Just a demo, does nothing useful

import config from "./config"
import SockJS from "sockjs-client";
import Stomp from "stomp-websocket";

const socket = new SockJS(config.ws_url);
export const stompClient = Stomp.over(socket);
let subscription;

stompClient.connect({}, function (frame) {
    stompClient.subscribe("/ws/post", function (msg) {
        console.log(msg);
    });
    subscription = stompClient.subscribe("/ws/message", function (msg) {
        console.log("Got message: " + msg.body)
    })
});


export const test = () => {
    stompClient.send("/wsapi/test", {}, JSON.stringify({text: "text1", id: 20}));
};

export const sendMessage = () => {
    stompClient.send("/wsapi/message", {}, "new message @" + new Date())
    subscription.unsubscribe();
};
