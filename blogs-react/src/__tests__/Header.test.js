import renderer from "react-test-renderer";
import Header from "../components/header";
import React from "react";
import {Link, Router, withRouter} from "react-router-dom";
import {Button} from "reactstrap";
import { createMemoryHistory as createHistory } from "history";
import {AuthContext} from "../components/auth-context";
import credentials from "../components/credential-storage";
test('shows header with filters', () => {
    const history=createHistory();
    const app = renderer.create(<Router history={history}><Header/></Router>);
    expect(app.root.findAll(el => el.type==="div").length).toBe(2);
    expect(app).toMatchSnapshot();
    history.push("/post");
});

test('shows header without filters', () => {
    const history=createHistory();
    const app = renderer.create(<Router history={history}><Header/></Router>);
    history.push("/post");
    expect(app.root.findAll(el => el.type==="div").length).toBe(1);
    expect(app).toMatchSnapshot();
});

test('shows header with filters and user', () => {
    credentials.access_token = "access_token";
    const history=createHistory();
    const app = renderer.create(<Router history={history}><Header/></Router>);
    expect(app.root.findAll(el => el.type==="div").length).toBe(4);
    expect(app).toMatchSnapshot();
});

test('shows header without filters and with user', () => {
    credentials.access_token = "access_token";
    const history=createHistory();
    const app = renderer.create(<Router history={history}><Header/></Router>);
    history.push("/post");
    expect(app.root.findAll(el => el.type==="div").length).toBe(3);
    expect(app).toMatchSnapshot();
});
