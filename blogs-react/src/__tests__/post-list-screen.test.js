import renderer from "react-test-renderer";
import React from "react";
import * as jest from "jest";
import PostsListScreen from "../screens/post-list-screen";
import {createMemoryHistory as createHistory} from "history";
import {Router} from "react-router-dom";

const posts= `
[
  {
    "id": 147,
    "createDate": "2019-01-18T15:23:13.569Z",
    "title": "rtrttete",
    "description": "tetetete",
    "content": "tetetee",
    "author": {
      "id": 135,
      "username": "google_kkit",
      "email": "kkit.subs@gmail.com",
      "fullName": "Yaroslav"
    },
    "topics": "|Travel|",
    "thumb": null,
    "pictureHeight": 0,
    "pictureWidth": 0
  },
  {
    "id": 146,
    "createDate": "2019-01-18T14:58:30.482Z",
    "title": "sasadkjassajkh",
    "description": "sakdasndkja",
    "content": "iuhiuhiuh",
    "author": {
      "id": 1,
      "username": "admin",
      "email": "admin@tt.tt",
      "fullName": "Admin"
    },
    "topics": "|Travel|",
    "thumb": null,
    "pictureHeight": 0,
    "pictureWidth": 0
  },
  {
    "id": 145,
    "createDate": "2019-01-18T14:57:55.332Z",
    "title": "asdsadas",
    "description": "dasdasd",
    "content": "asdasd",
    "author": {
      "id": 1,
      "username": "admin",
      "email": "admin@tt.tt",
      "fullName": "Admin"
    },
    "topics": "|Science|",
    "thumb": null,
    "pictureHeight": 0,
    "pictureWidth": 0
  }]
`;

// We'll mock RestClient.getBlog so that PostsListScreen don't call it
// But it's async
jest.mock("../RestClient");
const RestClient = require("../RestClient");

// getBlogs would go to fetch data from server, which we don't need, so we'll mock it here
RestClient.getBlogs.mockImplementation(() => {
    console.log("Mocked getBlogs");
    return Promise.resolve(new Response(posts, {status: 200}));
});

// Thanks to https://github.com/facebook/jest/issues/2157#issuecomment-279171856
// using this will let allow to wait until React component will get loaded, and all its State will be set up correctly.
function flushPromises() {
    return new Promise(resolve => setImmediate(resolve));
}

test('shows posts list', async () => {
    const history=createHistory();
    const list = renderer.create(<Router history={history}><PostsListScreen/></Router>);
    expect(list).toMatchSnapshot({}, "before load");
    expect(list.root.findAll(el => el.type==="div").length).toBe(4);
    expect(list.root.findAll(el => el.type==="div")[3].children[0]).toBe("Loading...");
    expect(list.root.findAll(el => el.type.name==="PostInList").length).toBe(0);
    await flushPromises(); // a lot of thing is going in the background like fetching posts (in componentDidMount) and setting State. we'll just wait until everything is finished.
    expect(list.root.findAll(el => el.type==="div")[3].children[0]).not.toBe("Loading...");
    expect(list.root.findAll(el => el.type==="div").length).toBe(7);
    expect(list.root.findAll(el => el.type.name==="PostInList").length).toBe(3);
    expect(list).toMatchSnapshot({},"after load");
});