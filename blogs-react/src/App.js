import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Redirect, Route} from "react-router-dom";
import Header from "./components/header";
import {AuthContext} from "./components/auth-context";
import PostsListScreen from "./screens/post-list-screen";
import LoginScreen from "./screens/login-screen";
import ViewPostScreen from "./screens/view-post-screen";
import {autoLogin, login, loginOAuth, logout} from "./RestClient";
import EditPostScreen from "./screens/edit-post-screen";


// import {Button} from "reactstrap";

class App extends Component {
    constructor(props) {
        super(props);
        this.state  = {
            loggedIn: false,
        }
    }
    componentWillMount() {
        let res = autoLogin();
        if (res.isLoggedIn)
            this.setState({loggedIn : true});
    }

    login = (user, pass) => {
        return login(user, pass)
            .then(() => {
                this.setState({
                    loggedIn : true,
                })
            });
    };

    loginOAuth = (response) => {
        return loginOAuth(response)
            .then(()=> {
                this.setState({
                    loggedIn: true
                });
            })
    };

    logout = () => {
        logout();
        this.setState({
            loggedIn : false,
        });
        window.location="/";
    };


    render() {
        let contextValue = {
            login: this.login,
            logout: this.logout,
            loginOAuth: this.loginOAuth,
            // filter by author and topic is set in header, but implemented in PostListScreen. We'll pass it in the Context
            filterByTopic: this.state.filterByTopic,
            setFilterByTopic: (topic => this.setState({filterByTopic : topic==="all" ? "" : topic})),
            filterByAuthor: this.state.filterByAuthor,
            setFilterByAuthor: (author => this.setState({filterByAuthor : author})),
        };

        document.title = "Blogs Service";
        return (
            <AuthContext.Provider value={contextValue}>
{/* Commented out, because it's not really needed
                <Button onClick={() => test()}>Click</Button>
                <Button onClick={() => sendMessage()}>Send message</Button>
*/}
                <Router>
                    <div className="App d-flex flex-column container-fluid p-0">
                        <header><Header/></header>
                        <Route path="/" exact component={PostsListScreen}/>
                        <Route path="/login" component={LoginScreen}/>
                        <Route path="/post/:id" exact component={ViewPostScreen}/>
                        <Route path="/post/edit/:id" render={({match, history}) => (
                            this.state.loggedIn ?
                                <EditPostScreen match={match} history={history}/> :
                                <Redirect to="/"/>
                        )}/>
                        <Route path="/createpost" render={({match, history}) => (
                            this.state.loggedIn ?
                                <EditPostScreen match={match} history={history}/> :
                                <Redirect to="/"/>
                        )}/>
                    </div>
                </Router>
            </AuthContext.Provider>
        );
    }
}

export default App;
