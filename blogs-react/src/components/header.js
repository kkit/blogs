import React, {Component} from 'react';
import {Link, Route} from "react-router-dom";
import {AuthContext} from "./auth-context";
import LogoutButton from "./logout-button";
import {Input} from "reactstrap";
import {topics} from "../config";
import credentials from "./credential-storage";

class Header extends Component {
    constructor(props) {
        super(props);
        this.timeout = {};
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
    }

    render() {
        // using Route here to show filter fields only in posts list (which is in "/" url)
        return (
            <AuthContext.Consumer>
                {({setFilterByTopic, setFilterByAuthor, filterByAuthor, filterByTopic}) => (
                    <div className="header d-flex flex-md-row flex-column bg-info justify-content-between align-items-center shadow-lg">
                        <Link className="btn btn-danger shadow" to="/">Home</Link>
                        <Route path="/" exact render={() =>
                            <div className="flex-grow-1 d-flex flex-sm-row flex-column pr-md-2 pl-md-2 pt-2 pb-2">
                                <SelectTopic value={filterByTopic} onChange={(ev) => {
                                    setFilterByTopic(ev.target.value);
                                }}/>
                                <Input defaultValue={filterByAuthor} onChange={(ev) => {
                                    clearTimeout(this.timeout);
                                    let value = ev.target.value;
                                    this.timeout = setTimeout(() => setFilterByAuthor(value), 400)
                                }}/>
                            </div>
                        }/>
                        {credentials.isLoggedIn ?
                            <>
                            <div>{`Logged in as ${credentials.userDisplayName}`}</div><div>
                                <Link className="btn btn-success shadow ml-2" to="/createpost">Create post</Link>
                                <LogoutButton className="btn-outline-light ml-2  shadow"/></div>
                            </>
                            :
                            <Link className="btn btn-danger shadow" to="/login">Login</Link>
                        }
                    </div>
                )}
            </AuthContext.Consumer>
        );
    }

}

function SelectTopic (props) {
    return (
        <Input style={{maxWidth:200}} defaultValue={props.value} onChange={props.onChange} type="select">
            <option value={"all"} key={"all"}>Show All</option>
            {topics.map(topic=><option value={topic.navbar} key={topic.name}>{topic.name}</option>)}
        </Input>
    );
}

export default Header;