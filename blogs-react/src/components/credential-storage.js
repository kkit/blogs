const storage = localStorage;

class CredentialStorage {

    constructor() {
        this.id_token="";// id_token from google
        this.access_token = "";
        this.refresh_token = ""; // if exists
        this.userId=""; // BlogsUser.id
        this.provider=""; // provider name (config.GOOGLE etc). our server if ""
        this.providerUserId=""; // id given to the user by OAuth provider. like "107080013339432109156" in Google
        this.userDisplayName=""; // current user's display name (usually it's fullName
        this.userEmail="";
    }

    get isLoggedIn() {
        return !!this.access_token;
    }

    storeCredentials() {
        storage.setItem("access_token", this.access_token);
        storage.setItem("refresh_token", this.refresh_token);
        storage.setItem("user_id", this.userId);
        storage.setItem("user_name", this.userDisplayName);
        storage.setItem("email", this.userEmail);
        storage.setItem("provider", this.provider);
        storage.setItem("provider_user_id", this.providerUserId);
    }

    loadCredentials() {
        this.access_token = storage.getItem("access_token");
        this.refresh_token = storage.getItem("refresh_token");
        this.userId = storage.getItem("user_id");
        this.userDisplayName = storage.getItem("user_name");
        this.userEmail = storage.getItem("email");
        this.provider = storage.getItem("provider");
        this.providerUserId = storage.getItem("provider_user_id");
    }

    clearCredentials() {
        storage.removeItem("access_token");
        storage.removeItem("refresh_token");
        storage.removeItem("user_id");
        storage.removeItem("user_name");
        storage.removeItem("email");
        storage.removeItem("provider");
        storage.removeItem("provider_user_id");
        this.access_token = "";
        this.refresh_token = "";
        this.userId = "";
        this.userDisplayName = "";
        this.userEmail = "";
        this.provider = "";
        this.providerUserId = "";
    }
}

const instance = new CredentialStorage();

export default instance;