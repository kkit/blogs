import React, {Component} from 'react';
import {AuthContext} from "./auth-context";
import {Button} from "reactstrap";

class LogoutButton extends Component {
    // noinspection JSUnusedGlobalSymbols
    static contextType = AuthContext;
    render() {
        return (
            <Button {...this.props} onClick={this.context.logout}>
                Logout
            </Button>
        );
    }
}

export default LogoutButton;