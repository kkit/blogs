import React from "react";

export const AuthContext = React.createContext(
    {
        login : (user, pass) => {},
        logout : () => {},
        loginOAuth: (response) => {},
        filterByTopic: "",
        filterByAuthor: "",
        setFilterByTopic: (topic) => {this.filterByTopic = topic},
        setFilterByAuthor: (author) => {this.filterByTopic = author},
    }
);