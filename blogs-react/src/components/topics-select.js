import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Input} from "reactstrap";
import {topics} from "../config";

// Something like react-select could be used here, but I'd like to use as much standard components as possible
class TopicsSelect extends Component {
    static propTypes = {
        selected: PropTypes.string,
        onChange: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {value: this.props.selected};
    }

    onChange = (event) => {
        let value = "";
        for (let option of event.target.options) {
            if (option.selected)
                value = value+ "|" + option.value + "|";
        }
        this.props.onChange(value);
    };

    render() {
        const options = topics.map((topic) =>
            <option key={topic.name} selected={this.props.selected && this.props.selected.includes("|"+topic.name+"|")}>{topic.name}</option>
        );
        // it's better to use value prop of Input, or else we'll get "Use the `defaultValue` or `value` props on <select> instead of setting `selected` on <option>" warning
        return (
            <Input {...this.props} type="select" onChange={this.onChange} multiple size={topics.length.toString()}>
                {options}
            </Input>
        );
    }
}

export default TopicsSelect;