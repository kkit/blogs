package org.slhouse.blogs;

import org.hamcrest.Matchers;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slhouse.blogs.model.BlogPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.ObjectContent;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureJsonTesters
public class BlogsControllerIT {
    @Value("${oauth.clientID}")
    String clientID;
    @Value("${oauth.clientSecret}")
    String clientSecret;
    @Value("${oauth.scopes:read,write}")
    String[] scopes;

    @LocalServerPort
    private int port;

    @Autowired
    JacksonTester<List<BlogPost>> json;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void insecurelogin() throws IOException {
        template.getRestTemplate().getInterceptors().clear();
        ResponseEntity<String> resp = template.getForEntity("/api/blogs", String.class);
        String body = resp.getBody();
        assertThat(body, Matchers.startsWith("[{")); // here goes json array
        ObjectContent<List<BlogPost>> posts = json.parse(body);
        ResponseEntity<String> resp1 = template.getForEntity("/api/blogs/-1", String.class);
        assertThat(resp1.getStatusCode(), is(HttpStatus.NOT_FOUND));
        Long id = posts.getObject().get(0).getId();
        resp1 = template.getForEntity("/api/blogs/" + id, String.class);
        assertThat(resp1.getStatusCode(), is(HttpStatus.OK));
        ResponseEntity<String> respsec = template.getForEntity("/api/sec", String.class);
        assertThat(respsec.getBody(), Matchers.startsWith("{\"error"));
        ResponseEntity<String> respsecuser = template.getForEntity("/api/sec", String.class);
        assertThat(respsecuser.getBody(), Matchers.startsWith("{\"error"));
        assertThat(respsecuser.getStatusCode(), is(HttpStatus.UNAUTHORIZED));

        resp = template.getForEntity("/", String.class);
        assertThat(resp.getStatusCode(), is(HttpStatus.OK));
        resp = template.getForEntity("/hello", String.class);
        assertThat(resp.getStatusCode(), is(HttpStatus.FOUND)); // tries to redirect
    }

    @Test
    public void secureLogin() throws JSONException {
        // get authToken
        ResponseEntity<String> entity = template.withBasicAuth(clientID, clientSecret).postForEntity("/oauth/token?grant_type=password&username=user&password=user",null, String.class);
        assertThat(entity.getStatusCode(), is(HttpStatus.BAD_REQUEST)); // wrong user/pass
        entity = template.withBasicAuth(clientID, clientSecret).postForEntity("/oauth/token?grant_type=password&username=admin&password=admin",null, String.class);
        assertNotNull(entity.getBody());
        JSONObject json = new JSONObject(entity.getBody());
        String token = json.getString("access_token");
        // set Authorization as a Header for the next requests
        template.getRestTemplate().setInterceptors(Collections.singletonList(((request, body, execution) -> {
            request.getHeaders().add("Authorization", "Bearer " + token);
            return execution.execute(request, body);
        })));
        ResponseEntity<String> res = template.getForEntity("/api/sec", String.class);
        assertThat(res.getStatusCode(), is(HttpStatus.OK));
        assertNotNull(res.getBody());
        assertThat(res.getBody(), startsWith("(/sec)"));

        ResponseEntity<String> resp = template.getForEntity("/", String.class);
        assertThat(resp.getStatusCode(), is(HttpStatus.OK));
        resp = template.getForEntity("/hello", String.class);
        assertThat(resp.getStatusCode(), is(HttpStatus.FOUND)); // redirect works. For it to work we need  org.apache.httpcomponents.httpclient as a dependency in .pom

        template.getRestTemplate().getInterceptors().clear(); // remove authorization interceptors afterwards
    }
}