package org.slhouse.blogs;

import org.apache.http.entity.ContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;
import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * @author Yaroslav V. Khazanov
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsersControllerIT {
    @Value("${oauth.clientID}")
    String clientID;
    @Value("${oauth.clientSecret}")
    String clientSecret;
    @Value("${oauth.scopes:read,write}")
    String[] scopes;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate template;
    private TestRestTemplate user;

    @Before
    public void setUp() throws Exception {
//        user = template.withBasicAuth("admin", "admin");
    }

    @Test // tyr access with unknown user, then create user, then try to access again.
    public void createUser() throws JSONException {
        String name = "testuser";
        String pass = "pass";
        // First we shouldn't get access_token
        ResponseEntity<String> entity = template.withBasicAuth(clientID, clientSecret).postForEntity("/oauth/token?grant_type=password&username="+name+"&password="+pass,null, String.class);
        assertThat(entity.getStatusCode(), is(HttpStatus.BAD_REQUEST)); // wrong user/pass

        // Anonymous should be able to create an account and then use it to get some /sec
        JSONObject json = new JSONObject();
        json.put("username", name);
        json.put("password", pass);
        json.put("email", "a@aa.aa");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", ContentType.APPLICATION_JSON.toString());
        ResponseEntity<ResponseEntity> post = template.exchange("/api/user", HttpMethod.POST, new HttpEntity<>(json.toString(), headers), ResponseEntity.class);
        assertThat(post.getStatusCode(), is(HttpStatus.OK));
        entity = template.withBasicAuth(clientID, clientSecret).postForEntity("/oauth/token?grant_type=password&username="+name+"&password="+pass,null, String.class);
        assertThat(entity.getStatusCode(), is(HttpStatus.OK));
        assertNotNull(entity.getBody());
        json = new JSONObject(entity.getBody());
        String token = json.getString("access_token");
        // set Authorization as a Header for the next requests
        template.getRestTemplate().setInterceptors(Collections.singletonList(((request, body, execution) -> {
            request.getHeaders().add("Authorization", "Bearer " + token);
            return execution.execute(request, body);
        })));
        ResponseEntity<String> res = template.getForEntity("/api/sec", String.class);
        assertThat(res.getStatusCode(), is(HttpStatus.OK));
        assertNotNull(res.getBody());
        assertThat(res.getBody(), startsWith("(/sec)"));

        // we can't create the same user again
        json = new JSONObject();
        json.put("username", name);
        json.put("password", pass);
        json.put("email", "a@aa.aa");
        headers = new HttpHeaders();
        headers.add("Content-Type", ContentType.APPLICATION_JSON.toString());
        ResponseEntity<String> err = template.exchange("/api/user", HttpMethod.POST, new HttpEntity<>(json.toString(), headers), String.class);
        assertThat(err.getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));


    }

}
