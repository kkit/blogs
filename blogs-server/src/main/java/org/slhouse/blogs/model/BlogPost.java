package org.slhouse.blogs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.slhouse.blogs.utils.ImageUtils;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.awt.*;
import java.time.Instant;
import java.util.Date;

/**
 * @author Yaroslav V. Khazanov
 **/
@Entity
@Data
@NoArgsConstructor
@Slf4j
public class BlogPost {
    private static int thumbSize=40;

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Instant createDate;
    @NotEmpty
    private String title;
    @NotEmpty
    private String description;
    @ToString.Exclude
    @Lob
    private String content;
    @ManyToOne(optional = false)
    @NotNull
    private BlogsUser author;
    @NotEmpty
    private String topics; // topics for the post, each in || chars. Like |topic1||topic2||etc|
//    Boolean published = false; // is the blog published, or still is a draft
    @ToString.Exclude
    @Lob @Basic(fetch = FetchType.LAZY)
    private byte[] picture;
    @ToString.Exclude
    @Lob
    private byte[] thumb; // small version of the picture - used in BlogsList
    // if picture exists, here we'll have its size
    private int pictureHeight = 0;
    private int pictureWidth = 0;

    public BlogPost(Instant createDate, String title, String description, String content, BlogsUser author, String topics) {
        this.createDate = createDate;
        this.title = title;
        this.description = description;
        this.content = content;
        this.author = author;
        this.topics = topics;
    }

    @JsonProperty
    public void setPicture(byte[] picture) {
        this.picture = picture;
        if (picture == null) {
            setPictureWidth(0);
            setPictureHeight(0);
            setThumb(null);
        } else {
            Dimension imageSize = ImageUtils.getImageSize(picture);
            setPictureWidth(imageSize.width);
            setPictureHeight(imageSize.height);
            setThumb(ImageUtils.resizeImageTo(picture, thumbSize, thumbSize));
        }
    }

    @JsonIgnore // we won't send the image to the client inside json. instead there will be separate request from client
    public byte[] getPicture() {
        return picture;
    }

    // picture height and width should go to a client, but on the server they are calculated, so don't accept them from a client
    @JsonProperty
    public int getPictureHeight() {
        if (picture == null)
            return 0;
        return pictureHeight;
    }

    @JsonIgnore
    private void setPictureHeight(int pictureHeight) {
        this.pictureHeight = pictureHeight;
    }

    @JsonProperty
    public int getPictureWidth() {
        if (picture == null)
            return 0;
        return pictureWidth;
    }

    @JsonIgnore
    private void setPictureWidth(int pictureWidth) {
        this.pictureWidth = pictureWidth;
    }
}
