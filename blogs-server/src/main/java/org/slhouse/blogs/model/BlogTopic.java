package org.slhouse.blogs.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Yaroslav V. Khazanov
 **/
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogTopic {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    Long id;
    @Column(nullable = false, unique = true)
    String name;

    public BlogTopic(String name) {
        this.name = name;
    }
}
