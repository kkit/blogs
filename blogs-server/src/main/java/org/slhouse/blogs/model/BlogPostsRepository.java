package org.slhouse.blogs.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author Yaroslav V. Khazanov
 **/
public interface BlogPostsRepository extends JpaRepository<BlogPost, Long>, JpaSpecificationExecutor<BlogPost> {
    // JpaSpecificationExecutor allows to user Specifications (and predicates) in findAll etc.
    List<BlogPost> findAllByTopicsContaining(String topic);
}
