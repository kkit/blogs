package org.slhouse.blogs.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TopicRepository extends JpaRepository<BlogTopic, Long> {
    Optional<BlogTopic> findByName(String name);
    BlogTopic getByName(String name);
}
