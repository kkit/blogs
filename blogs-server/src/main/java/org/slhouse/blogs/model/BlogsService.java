package org.slhouse.blogs.model;

import de.svenjacobs.loremipsum.LoremIpsum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yaroslav V. Khazanov
 **/
@Service
public class BlogsService {
    @Autowired
    BlogPostsRepository blogPostsRepository;

    public List<BlogPost> getBlogs(String topic, String author, Pageable pageable) {
        Page<BlogPost> page;
        if (topic==null && author==null)
          page = blogPostsRepository.findAll(pageable);
        else {
            page = blogPostsRepository.findAll(new Specification<BlogPost>() {
                @Override
                public Predicate toPredicate(Root<BlogPost> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                    List<Predicate> predicates = new ArrayList<>();
                    if (author != null)
                        predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("author").get("fullName")), "%"+author.toLowerCase()+"%")));
                    if (topic != null) {
                        predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("topics"), "%|" + topic + "|%")));
                    }
                    return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                }
            },
                    pageable);
        }
        return page.getContent();
    }
}
