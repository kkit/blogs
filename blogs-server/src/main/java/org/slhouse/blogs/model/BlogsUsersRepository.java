package org.slhouse.blogs.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Yaroslav V. Khazanov
 **/
public interface BlogsUsersRepository extends JpaRepository<BlogsUser, Long> {
    Optional<BlogsUser> findByUsername(String username);
    BlogsUser getByUsername(String username);
    List<BlogsUser> findAllByFullNameContains(String username);
    Optional<BlogsUser> findByAuthProviderAndProviderId(String provider, String id);

}
