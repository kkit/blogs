package org.slhouse.blogs.model;

import de.svenjacobs.loremipsum.LoremIpsum;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

/**
 * @author Yaroslav V. Khazanov
 **/
@Service
public class InitializationService {
    @Value("${initial.posts.count}")
    int initialPostsCount;

    @Autowired
    BlogPostsRepository blogPostsRepository;

    @Autowired
    TopicRepository topicRepository;

    @Autowired
    BlogsUsersRepository usersRepository;

    @Autowired
    private PasswordEncoder encoder;

    @PostConstruct
    public void init() throws IOException {
        // User base initialization
        int usersCount = 5;
        List<BlogsUser> users = new ArrayList<>();
        if (usersRepository.count() == 0) {
            BlogsUser user = new BlogsUser("admin", encoder.encode("admin"), "ROLE_ADMIN", "admin@tt.tt");
            user.setFullName("Admin");
            usersRepository.save(user);
            users.add(user);
            for (int i = 1; i <= usersCount; i++) {
                user = new BlogsUser("user" + i, encoder.encode("user"+i), "ROLE_USER", "user"+i+"@tt.tt");
                user.setFullName("User " + i + " Name");
                usersRepository.save(user);
                users.add(user);
            }
        }
        // Create default topics
        if (topicRepository.count() == 0) {
            topicRepository.save(new BlogTopic("Fashion"));
            topicRepository.save(new BlogTopic("Food"));
            topicRepository.save(new BlogTopic("Health"));
            topicRepository.save(new BlogTopic("Music"));
            topicRepository.save(new BlogTopic("Nature"));
            topicRepository.save(new BlogTopic("News"));
            topicRepository.save(new BlogTopic("Science"));
            topicRepository.save(new BlogTopic("Sports"));
            topicRepository.save(new BlogTopic("Travel"));
        }
        BlogsUser admin = usersRepository.getByUsername("admin");
        LoremIpsum lorem = new LoremIpsum();
        List<BlogTopic> allTopics = topicRepository.findAll();
        Random r = new Random();
        if (blogPostsRepository.count() == 0) {
            // Create some posts for a while
            int postsAmount = initialPostsCount;
            for (long i = 0; i <= postsAmount; i++) {
                StringBuilder topic = new StringBuilder();
                int amnt = r.nextInt(8)+1;
                for (int j = 0; j < amnt; j++) {
                    String newTopic = allTopics.get(r.nextInt(9)).getName();
                    if (!topic.toString().contains("|" + newTopic + "|"))
                      topic.append("|").append(newTopic).append("|");
                }
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.set(Calendar.DATE, r.nextInt(7)+1); // select first seven days of the current month randomly
                BlogPost post = new BlogPost(calendar.toInstant(), "(" + i + ") " + lorem.getRandomWords(5),
                        lorem.getRandomWords(15),
                        lorem.getParagraphs(10),
                        users.get(r.nextInt(6)),
                        topic.toString());
                byte[] picture = IOUtils.resourceToByteArray("/" + (r.nextInt(4)+1) +".jpg");
                post.setPicture(picture);
                blogPostsRepository.save(post);
            }
        }
    }
}

