package org.slhouse.blogs.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Yaroslav V. Khazanov
 * @see https://dzone.com/articles/spring-boot-hibernate-tips for annotations
 **/
@Entity
@Data
@EntityListeners(AuditingEntityListener.class) // needed in order to @CreatedDate and @LastModifiedDate to work
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class BlogsUser implements UserDetails {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CreatedDate
    LocalDateTime createdAt;
    @LastModifiedDate
    LocalDateTime updatedAt;
    @ToString.Exclude
    private String password;
    @Column(unique = true)
    private String username;
    @JsonIgnore
    private String authorities; // authorities divided by ,
    @JsonIgnore
    private boolean accountNonExpired = true;
    @JsonIgnore
    private boolean accountNonLocked = true;
    @JsonIgnore
    private boolean credentialsNonExpired = true;
    @JsonIgnore
    private boolean enabled = true;
    // Authorization via third party providers. These two fields are filled when user came from smth like google or facebook
    @JsonIgnore
    private String authProvider; // provider name
    @JsonIgnore
    private String providerId; // user id given by this provider. equals id if provider==OUR

    @Email @NotEmpty
    private String email;
    private String fullName;

    public BlogsUser() {
    }

    public BlogsUser(String username, String password, String authorities, String email) {
        this.password = password;
        this.username = username;
        this.authorities = authorities;
        this.email = email;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Stream.of(authorities.split(",")).map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }
}
