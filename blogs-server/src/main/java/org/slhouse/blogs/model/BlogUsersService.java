package org.slhouse.blogs.model;

import org.hibernate.exception.ConstraintViolationException;
import org.slhouse.blogs.api.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotEmpty;

/**
 * @author Yaroslav V. Khazanov
 **/
@Service
public class BlogUsersService implements UserDetailsService {

    @Autowired
    BlogsUsersRepository repository;
    @Autowired
    private PasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }

    @PostConstruct
    private void init() {
    }

    public UserDetails loadOAuth2User(String provider, String id) {
      return repository.findByAuthProviderAndProviderId(provider, id).orElse(null);
    }

    public BlogsUser createOAuthUser(String username, String password, String email, String provider, String provider_id, String fullName) {
        BlogsUser user = new BlogsUser(username, password, "ROLE_USER", email);
        user.setAuthProvider(provider);
        user.setProviderId(provider_id);
        user.setFullName(fullName);
        return repository.save(user);
    }

    public void createUser(String name, String password, String email) {
        BlogsUser user = new BlogsUser(name, encoder.encode(password), "ROLE_USER", email);
        user.setFullName("Created automatically");
        try {
            repository.save(user);
        } catch (RuntimeException ex) {
            if (ex.getCause() instanceof ConstraintViolationException)
                throw new UserAlreadyExistsException(name); // will be handled by UsersControllerExceptionHandler
            else
                throw ex;
        }
    }

    public void createUser(String name, String password, @NotEmpty String email, String fullName) {
        BlogsUser user = new BlogsUser(name, encoder.encode(password), "ROLE_USER", email);
        user.setFullName(fullName);
        try {
            repository.save(user);
        } catch (RuntimeException ex) {
            if (ex.getCause() instanceof ConstraintViolationException)
                throw new UserAlreadyExistsException(name); // will be handled by UsersControllerExceptionHandler
            else
                throw ex;
        }
    }
}
