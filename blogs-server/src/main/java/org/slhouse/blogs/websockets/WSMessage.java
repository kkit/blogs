package org.slhouse.blogs.websockets;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Yaroslav V. Khazanov
 * WebSockets message
 **/
@AllArgsConstructor
@Getter
@Setter
public class WSMessage {
    String text;
    Long id;
}
