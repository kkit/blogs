package org.slhouse.blogs.websockets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

/**
 * @author Yaroslav V. Khazanov
 * Some test controller, just for a demo
 **/
@Controller
public class MessageController {
    @MessageMapping("/test")
    @SendTo("/ws/post")
    public WSMessage message(WSMessage wsMessage) throws InterruptedException {
        Thread.sleep(2000);
        return new WSMessage(wsMessage.getText(), wsMessage.getId());
    }

    @Autowired
    SimpMessagingTemplate template;

    @MessageMapping("/message")
    public void sendMessage(String msg) {
        template.convertAndSend("/ws/message", msg);
    }

}
