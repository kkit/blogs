package org.slhouse.blogs.config;

import org.slhouse.blogs.model.BlogUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Yaroslav V. Khazanov
 **/
@Configuration
@EnableWebSecurity(debug = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    BlogUsersService blogUsersService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(blogUsersService);
    }

    // used in AuthorizationServerConfig
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/wsapi/**","/stomp/**").permitAll()
                .antMatchers("/", "/home","/error**").permitAll()
                .antMatchers("/h2/**").permitAll() // needed for h2 console
                .antMatchers("/hello").authenticated()
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").permitAll()// not needed here, but possible .successHandler(new OauthAuthenticationSuccessHandler())
                .and()
                .logout().permitAll()
                .and()
                .csrf().disable() //.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and() // disabled for h2 console
                .headers().frameOptions().disable()// needed for h2 console
                .and()
                .httpBasic()
                .and()
                .oauth2Login().loginPage("/loginauth")// we actually don't want this page, because we'll put Google button on the usual login page
//                .successHandler(new OauthAuthenticationSuccessHandler())// but this one still will work when we'll login via google or smth like that

        ;

    }
}
