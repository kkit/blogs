package org.slhouse.blogs.config;

/**
 * @author Yaroslav V. Khazanov
 * Suported Auth providers
 **/
public enum SupportedAuthProvider {
    OUR(""), GOOGLE("google");

    private final String name;

    public String getName() {
        return name;
    }

    SupportedAuthProvider(String name) {
        this.name = name;
    }

}
