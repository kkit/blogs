package org.slhouse.blogs.config;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import lombok.extern.slf4j.Slf4j;
import org.slhouse.blogs.model.BlogUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Yaroslav V. Khazanov
 * Takes google's id_token and uses it to authenticate a user on OUR authentication server.
 * adds the user to our user DB
 **/
@Component
@Slf4j
public class GoogleTokenGranter extends ResourceOwnerPasswordTokenGranter {
    @Value("${spring.security.oauth2.client.registration.google.client-id}")
    private String clientId;
    @Value("${spring.security.oauth2.client.registration.google.client-secret}")
    private String clientSecret;
    private static final String GRANT_TYPE = SupportedAuthProvider.GOOGLE.getName();
    private final SupportedAuthProvider supportedAuthProvider = SupportedAuthProvider.GOOGLE;

    @Autowired
    BlogUsersService usersService;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    public GoogleTokenGranter(AuthenticationManager authenticationManager, AuthorizationServerTokenServices authorizationServerTokenServices, ClientDetailsService clientDetailsService) {
        super(authenticationManager, authorizationServerTokenServices, clientDetailsService, new DefaultOAuth2RequestFactory(clientDetailsService), GRANT_TYPE);
    }

        @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        try {
            String id_token = tokenRequest.getRequestParameters().get("google_id_token");
            GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new JacksonFactory())
                    .setAudience(Collections.singletonList(clientId)).build();
            GoogleIdToken verify = verifier.verify(id_token);
            // search for user in user base. if not found, create it, where login is grant_type + id, password = random.
            // then try to log in as usual (in super)
            String userId = verify.getPayload().get("sub").toString();
            UserDetails auth2User = usersService.loadOAuth2User(supportedAuthProvider.getName(), userId);
            String userName = GRANT_TYPE + " " + userId;
            String password = UUID.randomUUID().toString();
            if (auth2User == null)
                auth2User = usersService.createOAuthUser(userName, passwordEncoder.encode(password), verify.getPayload().getEmail(), supportedAuthProvider.getName(), userId, verify.getPayload().get("name").toString());
            UsernamePasswordAuthenticationToken userAuth = new UsernamePasswordAuthenticationToken(userName, password);

            Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
            userAuth.setDetails(parameters);

            try {
                // we don't really need any authentication here because it's actually done in GoogleIdTokenVerifier
                // so we'll just fill the needed fields
//                userAuth = authenticationManager.authenticate(userAuth);
                userAuth = new UsernamePasswordAuthenticationToken(auth2User, null, Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
                parameters.put("provider", GRANT_TYPE);
                parameters.put("provider_id", userId);
                userAuth.setDetails(parameters);
            }
            catch (AccountStatusException | BadCredentialsException ase) {
                //covers expired, locked, disabled cases (mentioned in section 5.2, draft 31)
                throw new InvalidGrantException(ase.getMessage());
            }
            // If the username/password are wrong the spec says we should send 400/invalid grant
            if (!userAuth.isAuthenticated()) {
                throw new InvalidGrantException("Could not authenticate user: " + userName);
            }
            OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
            return new OAuth2Authentication(storedOAuth2Request, userAuth);
        } catch (GeneralSecurityException | IOException e) {
            log.error("Error in Google authentication process", e);
            return null;
        }
    }

}
