package org.slhouse.blogs.config;

import org.slhouse.blogs.model.BlogsUser;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Yaroslav V. Khazanov
 * Adds addtiional information to the access token - e.g. BlogUser's id. It will help us to find the user later
 **/
public class JWTTokenEnhancer implements TokenEnhancer {
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        if (authentication.getPrincipal() instanceof BlogsUser) {
            BlogsUser user = (BlogsUser) authentication.getPrincipal();
            Map<String, Object> addInfo = new HashMap<>();
            addInfo.put("id", user.getId()); // it's now stored in plain text in json that comes to a client with access_token.
            addInfo.put("fullName", user.getFullName());
            addInfo.put("email", user.getEmail());
            Map<String, String> det = (Map<String, String>) authentication.getUserAuthentication().getDetails();
            addInfo.put("provider", det.get("provider"));
            addInfo.put("provider_id", det.get("provider_id"));
            ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(addInfo);
        }
        return accessToken;
    }
}
