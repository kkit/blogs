package org.slhouse.blogs.config;

import org.slhouse.blogs.model.BlogUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.token.*;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author Yaroslav V. Khazanov
 **/
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
    @Value("${oauth.clientID}")
    String clientID;
    @Value("${oauth.clientSecret}")
    String clientSecret;
    @Value("${oauth.scopes:read,write}")
    String[] scopes;

    @Autowired
    AuthenticationManager manager; // needed in order to grant_type=password to work
    @Autowired
    JwtAccessTokenConverter converter;
    @Autowired
    UserAuthenticationConverter userAuthenticationConverter;
    @Autowired
    BlogUsersService userDetailsService;

    @Autowired
    GoogleTokenGranter googleTokenGranter;

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
        // thanks to https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#mvc-cors-filter
        // this filter is needed to React Application using /oauth to get auth_token
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedMethod("*");
        config.addAllowedHeader("*");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/oauth/**", config);
        security.addTokenEndpointAuthenticationFilter(new CorsFilter(source));
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        TokenEnhancerChain chain = new TokenEnhancerChain();
        chain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), converter));
        endpoints.tokenStore(tokenStore()).tokenEnhancer(chain).authenticationManager(manager);
        // Now here add all other OAuth2 providers like Facebook, Github etc.
        CompositeTokenGranter compositeTokenGranter = new CompositeTokenGranter(Collections.singletonList(endpoints.getTokenGranter()));
        compositeTokenGranter.addTokenGranter(googleTokenGranter);
        endpoints.tokenGranter(compositeTokenGranter);
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory().withClient(clientID).secret(new BCryptPasswordEncoder().encode(clientSecret)).scopes(scopes)
                .authorizedGrantTypes("password", "refresh_token", "google").accessTokenValiditySeconds(20000)
                .refreshTokenValiditySeconds(20000);
    }

    @Bean
    UserAuthenticationConverter userAuthenticationConverter() {
        DefaultUserAuthenticationConverter converter = new DefaultUserAuthenticationConverter();
        converter.setUserDetailsService(userDetailsService);
        return converter;
    }

    @Bean
    public TokenEnhancer tokenEnhancer() {
        return new JWTTokenEnhancer();
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(converter);
    }

    @Bean
    @Primary
    public AuthorizationServerTokenServices authorizationTokenServices() {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        JWTTokenConverter tokenConverter = new JWTTokenConverter(userDetailsService);
        tokenServices.setTokenStore(new JwtTokenStore(tokenConverter));
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setReuseRefreshToken(false);
        TokenEnhancerChain chain = new TokenEnhancerChain();
        chain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), tokenConverter));
        tokenServices.setTokenEnhancer(chain);
        return tokenServices;
    }
}
