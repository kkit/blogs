package org.slhouse.blogs.config;

import org.slhouse.blogs.model.BlogUsersService;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Yaroslav V. Khazanov
 **/
@Component
@Primary
public class JWTTokenConverter extends JwtAccessTokenConverter {

    public JWTTokenConverter(BlogUsersService blogUsersService) {
        super();
        setSigningKey("jwtkey"); // we could skip it or use assymetric keys for sign/verify
        // these lines are needed to have @AuthenticationPrincipal Principal principal in Controllers filled with BlogsUser
        DefaultAccessTokenConverter tokenConverter = new DefaultAccessTokenConverter();
        DefaultUserAuthenticationConverter authenticationConverter = new DefaultUserAuthenticationConverter();
        authenticationConverter.setUserDetailsService(blogUsersService); // we need to have here correct UsersService
        tokenConverter.setUserTokenConverter(authenticationConverter);
        setAccessTokenConverter(tokenConverter);
    }

    @Override // this allows us to have additional info added in JWTTokenEnhancer
    public OAuth2Authentication extractAuthentication(Map<String, ?> claims) {
        OAuth2Authentication authentication = super.extractAuthentication(claims);
        authentication.setDetails(claims);
        return authentication;
    }
}
