package org.slhouse.blogs.api;

/**
 * @author Yaroslav V. Khazanov
 **/
public class UserAlreadyExistsException extends RuntimeException {
    private final String name;

    public UserAlreadyExistsException(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
