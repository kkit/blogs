package org.slhouse.blogs.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Yaroslav V. Khazanov
 **/
@RestControllerAdvice
public class UsersControllerExceptionHandler {

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity handleUserNotFound(UserAlreadyExistsException exception) {
        return ResponseEntity.unprocessableEntity().body("User already exists: " + exception.getName());
    }
}
