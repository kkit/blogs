package org.slhouse.blogs.api;

import org.slhouse.blogs.model.BlogUsersService;
import org.slhouse.blogs.model.BlogsUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * @author Yaroslav V. Khazanov
 **/
@RestController
@RequestMapping("/api")
public class UsersController {
    @Autowired
    BlogUsersService service;

    @PostMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createUser(@RequestBody @Valid BlogsUser user) {
//        service.createUser(json.get("name"), json.get("password"), json.get("email"), json.get("fullName"));
        service.createUser(user.getUsername(), user.getPassword(), user.getEmail(), user.getFullName());
        return ResponseEntity.ok().build();
    }
}
