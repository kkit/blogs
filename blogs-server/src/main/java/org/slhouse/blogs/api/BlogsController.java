package org.slhouse.blogs.api;

import org.slhouse.blogs.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.Instant;
import java.util.List;

/**
 * @author Yaroslav V. Khazanov
 **/
@RestController
@RequestMapping("/api")
public class BlogsController {
    private static int PAGE_SIZE = 20;

    @Autowired
    BlogPostsRepository blogPostsRepository;

    @Autowired
    BlogsService blogsService;

    @Autowired
    TopicRepository topicRepository;

/*
    @Autowired
    SimpMessagingTemplate template;
*/


    @GetMapping("/sec")
    public String root(@AuthenticationPrincipal Principal principal) {
        return "(/sec) Hello to " + (principal == null ? "anonymous" : principal.getName()) + " from Blogs Controller @" + System.currentTimeMillis();
    }

    @GetMapping("/blogs")
    public List<BlogPost> listBlogs(@AuthenticationPrincipal Principal principal,
                                    @RequestParam(value = "topic", required = false) String topic,
                                    @RequestParam(value = "author", required = false) String author,
                                    @RequestParam(value = "page", required = false, defaultValue = "0") Integer pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum, PAGE_SIZE, Sort.by(Sort.Direction.DESC, "createDate"));
        return blogsService.getBlogs(topic, author, pageRequest);
//        return blogPostsRepository.findAll(Sort.by(Sort.Order.asc("createDate")));
    }

    @GetMapping("/blogs/{id}")
    public ResponseEntity<BlogPost> getPost(@AuthenticationPrincipal Principal principal, @PathVariable Long id) {
        return blogPostsRepository.findById(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/blogs/{id}/image", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getPostImage(@AuthenticationPrincipal Principal principal, @PathVariable Long id) {
        return blogPostsRepository.findById(id).map(post -> ResponseEntity.ok(post.getPicture())).orElse(ResponseEntity.notFound().build());
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/blogs", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BlogPost> updatePost(@AuthenticationPrincipal Authentication principal, @RequestBody BlogPost post) {
        if (principal == null)
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        // todo check fields validity and that user is author of the post
        BlogsUser user = (BlogsUser) principal.getPrincipal();
        post.setAuthor(user);
        post.setCreateDate(Instant.now());
        BlogPost save = blogPostsRepository.save(post);
//        not really used, left here for demo purposes only
//        template.convertAndSend("/ws/post_changed", save);
        return ResponseEntity.ok(save);
    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping(value = "/blogs/{id}")
    public ResponseEntity deletePost(@AuthenticationPrincipal Authentication principal, @PathVariable Long id) {
        if (principal == null)  // should never happen
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        BlogsUser user = (BlogsUser) principal.getPrincipal();
        // check if the post belongs to user
        BlogPost post = blogPostsRepository.getOne(id);
        if (!post.getAuthor().getId().equals(user.getId()))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You're are not allowed to delete this post");
        blogPostsRepository.deleteById(id);
//        not really used, left here for demo purposes only
//        template.convertAndSend("/ws/post_deleted", post.getTitle());
        return ResponseEntity.ok().build();
    }

}
