package org.slhouse.blogs.utils;

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @author Yaroslav V. Khazanov
 * Takes an image and resizes it saving it's scale
 **/
public class ImageUtils {
    public static byte[] resizeImageTo(byte[] image, int maxWidth, int maxHeight) {
        Dimension d = getImageSize(image);
        double k = maxWidth > maxHeight ? maxHeight / d.getHeight() : maxWidth / d.getWidth();
        Dimension targetDimension = new Dimension();
        targetDimension.setSize(d.getWidth() * k, d.getHeight() * k);
        try (ByteInputStream input = new ByteInputStream(image, image.length)) {
            BufferedImage inImg = ImageIO.read(input);
            BufferedImage outImg = new BufferedImage(targetDimension.width, targetDimension.height, inImg.getType());
            Graphics2D g = outImg.createGraphics();
            g.setComposite(AlphaComposite.Src);
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.drawImage(inImg, 0, 0, targetDimension.width, targetDimension.height, null );
            g.dispose();
            ByteOutputStream out = new ByteOutputStream();
            ImageIO.write(outImg,"jpg",  out);
            return out.getBytes();
        } catch (IOException e) {
            throw new RuntimeException("Can't resize picture");
        }
    }

    public static Dimension getImageSize(byte[] image) {
        Dimension res = new Dimension();
        try (ByteInputStream input = new ByteInputStream(image, image.length)) {
            BufferedImage pic = ImageIO.read(input);
            res.setSize(pic.getWidth(), pic.getHeight());
            return res;
        } catch (IOException e) {
            throw new RuntimeException("Can't read picture");
        }
    }
}
