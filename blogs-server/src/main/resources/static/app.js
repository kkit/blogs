var stompClient = null;

window.onload = function () { connect(); }

function connect() {
    var socket = new SockJS('/todo-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/todos', function (todo) {
            location.reload(false);
        })
    })
}

function send(msg) {
    stompClient.send("/app/newtodo", {}, "new todo!");

}